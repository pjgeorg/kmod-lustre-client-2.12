include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'

default:
  image: registry.gitlab.com/centos/kmods/ci-image:latest

stages:
  - build

mock:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/
      when: never
    - when: always
  parallel:
    matrix:
      - ARCH: [x86_64]
  stage: build
  script:
    - |
      if [ -e sources ]
      then
        while read -r TYPE FILE _ HASH
        do
          if [ -z "$TYPE" ] || [ -z "$FILE" ] || [ -z "$HASH" ]
          then
            continue
          fi
          FILE=${FILE#(}
          FILE=${FILE%)}
          TYPE=${TYPE,,}
          if [ ! -e "$FILE" ]
          then
            curl -f -s -L -H Pragma: -o "./$FILE" -R --retry 5 "https://git.centos.org/sources/$CI_PROJECT_NAME/$FILE/$TYPE/$HASH/$FILE" && continue || true
            curl -f -s -S -L -H Pragma: -o "./$FILE" -R --retry 5 "https://src.fedoraproject.org/repo/pkgs/$CI_PROJECT_NAME/$FILE/$TYPE/$HASH/$FILE"
          fi
          echo "$HASH $FILE" | "${TYPE}sum" --check --status
        done < sources
      fi
    - mock -r centos-stream-8-$ARCH --define 'dist .el8s' --buildsrpm --spec *.spec --source . --resultdir .
    - |
      if [ -e sources ]
      then
        while read -r _ FILE _ _
        do
          if [ -z "$FILE" ]
          then
            continue
          fi
          FILE=${FILE#(}
          FILE=${FILE%)}
          if [ -e "$FILE" ]
          then
            rm -f $FILE
          fi
        done < sources
      fi
    - mock -r centos-stream-8-$ARCH --define 'dist .el8s' --resultdir . *.src.rpm
  artifacts:
    name: "$CI_PROJECT_NAME-mock-$ARCH"
    untracked: true
    when: always

cbs:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/
  stage: build
  script:
    - NAME=$(rpmspec -q --srpm --define 'dist .el8s' --queryformat='%{name}\n' *.spec)
    - NVR=$(rpmspec -q --srpm --define 'dist .el8s' --queryformat='%{name}-%{version}-%{release}\n' *.spec)
    - |
      if [ -e sources ]
      then
        while read -r TYPE FILE _ HASH
        do
          if [ -z "$TYPE" ] || [ -z "$FILE" ] || [ -z "$HASH" ]
          then
            continue
          fi
          FILE=${FILE#(}
          FILE=${FILE%)}
          TYPE=${TYPE,,}
          if [ ! -e "$FILE" ]
          then
            curl -f -s -L -H Pragma: -I -o /dev/null -R --retry 5 "https://git.centos.org/sources/$CI_PROJECT_NAME/$FILE/$TYPE/$HASH/$FILE" && continue || true
            curl -f -s -S -L -H Pragma: -o "./$FILE" -R --retry 5 "https://src.fedoraproject.org/repo/pkgs/$CI_PROJECT_NAME/$FILE/$TYPE/$HASH/$FILE"
            echo "$HASH $FILE" | "${TYPE}sum" --check --status
            curl -f -s -S -L --cert $CBS --form "name=$CI_PROJECT_NAME" --form "hash=$TYPE" --form "${TYPE}sum=$HASH" --form "file=@$FILE" "https://git.centos.org/sources/upload_sig.cgi"
          fi
        done < sources
      fi
    - 'cbs --cert=$CBS build --wait --scratch kmods8s-packages-main-el8s git+$CI_PROJECT_URL\#$CI_COMMIT_SHA'
    - 'cbs buildinfo $NVR 2>/dev/null | grep -q "State: COMPLETE" || cbs --cert=$CBS build --wait kmods8s-packages-main-el8s git+$CI_PROJECT_URL\#$CI_COMMIT_SHA'
    - 'cbs list-tagged kmods8s-packages-main-testing $NAME | grep -q $NVR || cbs --cert=$CBS tag-build kmods8s-packages-main-testing $NVR'
    - 'cbs list-tagged kmods8s-packages-main-release $NAME | grep -q $NVR || cbs --cert=$CBS tag-build kmods8s-packages-main-release $NVR'

kabi:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_NAMESPACE =~ /CentOS\/kmods\/.*/ && $CI_COMMIT_REF_NAME == "c8s/main"
  stage: build
  before_script:
    - git config --global user.email 'sig-kmods@centosproject.org'
    - git config --global user.name 'Kmods SIG'
  script:
    - |
      if [[ "$(git rev-parse HEAD)" != "$(git rev-parse origin/${CI_COMMIT_REF_NAME})" ]]
      then
        echo "Only run on HEAD of branch ${CI_COMMIT_REF_NAME}"
        exit 0
      fi
    - NAME=$(rpmspec -q --srpm --define 'dist .el8s' --queryformat='%{name}\n' *.spec)
    - NVR=$(rpmspec -q --srpm --define 'dist .el8s' --queryformat='%{name}-%{version}-%{release}\n' *.spec)
    - 'cbs buildinfo $NVR 2>/dev/null | grep -q "State: COMPLETE" || exit 1'
    - KERNEL=$(sed -rn "s/%global\s*kernel\s*(.*)$/\1/p" *.spec)
    - CKERNEL=$KERNEL
    - git clone --quiet --branch c8s --single-branch https://gitlab.com/CentOS/kmods/kabi
    - |
      for KVERSION in $(git -C kabi tag -l | sed -rn "s/^c8s\/(.*)\$/\1/p" | sort -V | sed -e "1,/$KERNEL/d")
      do
        for ARCH in $(grep "ExclusiveArch" *.spec | awk -F ":" '{print $2}')
        do
            RPM="https://cbs.centos.org/kojifiles/packages/$(rpmspec -q --define "dist .el8s" --queryformat="%{name}/%{version}/%{release}/$ARCH/%{name}-%{version}-%{release}.$ARCH.rpm\n" *.spec)"
            curl -f -s -I -o /dev/null $RPM
            while read -r SVERSION SYMBOL
            do
              if ! git -C kabi show c8s/$KVERSION:Module.symvers.$ARCH | grep "^$SVERSION\s*$SYMBOL" >/dev/null
              then
                [[ ${PIPESTATUS[0]} -eq 0 ]]
                KERNEL=$KVERSION
                break 3
              fi
            done < <(curl -f -s -N $RPM | rpm -qp --requires - | sed -rn 's@^kernel\((.*)\) = (0x[0-9a-f]*)$@\2\t\1@p' | sort -k2)
        done
      done
      if [[ "$CKERNEL" != "$KERNEL" ]]
      then
        git push -o ci.skip $(echo $CI_PROJECT_URL | sed "s/https:\/\//&kmods:$GITLAB@/") HEAD:refs/heads/c8s/$CKERNEL
        sed -i "s/%global\s*kernel\s*.*/%global kernel $KERNEL/" *.spec
        sed -i "s/%global\s*baserelease\s*.*/%global baserelease 0/" *.spec
        sed -i '/^%changelog$/q' *.spec
        rpmdev-bumpspec -c "kABI tracking kmod package (kernel >= $KERNEL)" -D *.spec
        sed -i -e :a -e '/^\n*$/{$d;N;};/\n$/ba' *.spec
        git add *.spec
        git commit -m "kABI tracking kmod package (kernel >= $KERNEL)"
        git push $(echo $CI_PROJECT_URL | sed "s/https:\/\//&kmods:$GITLAB@/") HEAD:refs/heads/c8s/main
      fi
